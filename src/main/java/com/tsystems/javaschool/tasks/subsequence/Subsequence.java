package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.lang.IllegalArgumentException;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null | y == null) {
            throw new IllegalArgumentException();
        }

        List<Object> z = new ArrayList<>();

        for (Iterator it = y.iterator(); it.hasNext();) {
            Object value = it.next();

            for (Iterator itInner = x.iterator(); itInner.hasNext();) {
                Object valueInner = itInner.next();
                if ((value == valueInner) & !z.contains(value)) {
                    z.add(value);
                }
            }
        }

        return x.equals(z);
    }
}
